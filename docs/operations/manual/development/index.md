---
title: Development Manual
description: No Fuss Computings Development Manual and Contribution Guide.
date: 2022-08-25
template: manual.html
about: https://gitlab.com/nofusscomputing
---


This Development manual pertains to all development within No Fuss Computing. It's also doubles as our contribution guide and is designed to cover all of the generic points for all of our projects. Each project will contain it's own contribution guide that will cover any specifics for that project. Both of these documents should be read in conjunction to one another.

There may be instances where multiple Contribution guides are referenced within a repository. In the case of conflicting guides the order of precedence for multiple contribution guides is as follows:

1. Repository
2. Project
3. This Document.

if a case arises, in any of our repositories wherein no contribution guide exists, this contribution guide is to be followed.

_TLDR. If you are not a developer, please see [issues](). If you are a developer, no TLDR for you, please read on._

Any project where you can see the repository, generally our open source projects. You can contribute to. The following assumptions are made for you to be able to contribute:

- you know what git is

- you know how to use gitlab

If you do not fall into any of the listed assumptions, it is required that you obtain this knowledge if you would like to conduct the change yourself. However you can still contribute, by raising an issue under the issue tab in the subject repository.

Further information can be found in the manual at these locations:

- [Git Branch usage and setup](repositories.md#git-branches)
- [Our Usage of Badges](badges.md)
- [Quality Assurance at No Fuss Computing](../quality_assurance/index.md)
- [Repository Setup](repositories.md)


## Workflow

> What git workflow do we use?

Git forking workflow.

So what now? Essentially any contributor who wishes to conduct the contribution themselves, will fork the repository.

> **Tip:** _setup your fork to mirror the parent repo to keep your branches up to date. To reduce the likelihood of a merge conflict, ensure that your `development` branch is kept up to date._

Generally or workflow is as follows:


**Planning Stage:**

   1. If a new feature, as part of the issue, write the full specification for said feature. This is required prior to commencing the next stages in development.


**Development Stage:**

   1. Fork the repository

   1. Create a `feature` branch using the `development` branch as the source

   1. Create a Merge Request using one of the supplied templates to the source repository `development` branch

   1. Clone your `feature` branch locally

   1. Do your work and conduct regular [commits](#Commits) as described below

   1. Push your local changes back to your fork

   1. When complete, cc @jon_nfc for code review. If all is well, your Merge Request will pass and be merged.

**Code Review Stage:**

   1. Code review will be completed by a dev. If the review found no issues to fix, the MR will be approved. If there are issues, please see any comments left for your action/collaboration.

**Completion:**

   1. Once your Merge Request is approved, a developer will merge your Merge Request, deleting your feature branch.

   1. If you no longer intend on contributing, you are free to delete your copy of the forked repository.


### Commits

We use [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) format and as such will require your commit messages to be formatted in accordance with the following.

``` text
Type(feature_name): descriptive summary
{Blank line}
More detailed description that explains reasons.
Can also be multiline, with blank line seperstions.
{Blank line}
{Issue #number} {MR!number}
```

Line one: ensure this summary makes sense when read with no other information. It will be used as the change log entry.

Line two: must be a blank empty line. This line is optional if no line three detailed description is supplied.

Line three: optional but suggested. this is where you put a detailed description of the commit.

Second last line: must be a blank empty line

Last line: this is the commit footer. This is where references to the Gitlab issue and/or merge request is placed. These references are mandatory. If you fail to put these in a commit message, the commit will fail [validation](https://gitlab.com/nofusscomputing/projects/gitlab-ci/-/tree/master/conventional_commits#conventional-commits-user-manual). These references will be used in the change log entry.

All commits will be [validated](https://gitlab.com/nofusscomputing/projects/gitlab-ci/-/tree/master/conventional_commits#conventional-commits-user-manual) to ensure compliance with the [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) format. If they fail the validation, you will see the test report within the Merge Request Interface.

Instead of providing examples of commit messages, please refer to the commits tab within the repository. You will see many examples of what a verified commit message looks like.

> Navigate to Menu -> Repository -> Commits


## Issues

We use issues as the method to develop the plan to implement or for a user to report an issue. If the former, no merge request is to be created until the planning progress is complete. For the latter, report your issue as required and using the provided template. Please complete as much of the template as possible, as this enables the dev team to confirm the validity of your report.

Time tracking is used during the planning stage on issues and is optional for non project members.


## Merge Requests

We use the [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) format for the title of the Merge Request. This provides us with the option to `squash` the commits during the merge operation. This will also be validated as part of the [CD/CI validation](https://gitlab.com/nofusscomputing/projects/gitlab-ci/-/tree/master/#ci-stages) stage. A project member may fix a bug without an issue. However the full details must be recorded in the merge request description and the merge request be referenced in the commit footer.

Time tracking is used on Merge Requests during the development and code review stage and is optional for non project members.


## Tasks Lists

When an issue is used for planning a feature, ensure a task list is utilised so that all requirements for the planned feature can be checked and confirmed.
This also assists during the development and code review stage as a developer should tick them as complete for every commit that addresses the task. As soon as a task is marked as complete leave a comment along the lines of `tasked completed in commit {commit hash}`.  
In the history, you will be able to see the point of completion for the task and immediately following a comment pointing to the commit completing the task. This ensures traceability and confirmation during code review.

Merge requests should also have a tasks list. It can be copied over from the related issue and the comments as mentioned above completed in the merge request, with the issue tasks all marked as complete when the work on the merge request is complete. With the issue having a comment of `tasks completed in MR !{MR number}`


## Labels

We use labels on both issues and merge requests. Labels are to be applied by project members and is used by us to categorise and prioritise our effort. If a label has been applied, do not make any label adjustments as this may cause delay in us addressing the issue/merge request.

You can see what each of the labels mean in our [labels](../quality_assurance/labels.md) document
