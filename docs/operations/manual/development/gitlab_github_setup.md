---
title: Gitlab / Github Setup
description: How NFC sets up their Gitlab and Github repositories.
date: 2022-08-25
template: manual.html
about: https://gitlab.com/nofusscomputing
---


As we host our repositories on Gitlab and Github, within this section is the settings that apply to the respective setup.

By default, our projects are hosted on Gitlab. When we do this and if open-sourced, we will mirror the repository to Github as well. Even though you can view and interact with the project on Github, all work is to be done from Gitlab.

Each tabbed section is designed to be cut and past to markdown, so that auditing can occur

=== "Gitlab"

    ```md
    - Protected Branches:

      - [ ] master* _protected_

      - [ ] development _protected_

    - [ ] Default branch: development

    - Merge Requests

      - [ ] All threads have to be resolved to merge

      - [ ] All Pipelines must succeed to mergemerge
    ```

    We utilise labels within gitlab. The details of this can be found within the [Quality Assurance](../quality_assurance/labels.md) manual.

=== "Github"

    these settings are yet to be placed.


### CI / CD Jobs

As we primarily use Gitlab, CI/CD jobs for all repositories that contain a product, will have jobs configured. To ease the burden of maintaining countless versions of job definitions, our repositories will use our [gitlab-ci project](../../../projects/gitlab-ci) as a git submodule. This enables a single place for all of our job definitions.

