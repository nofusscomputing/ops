---
title: Repository Setup
description: How NFC sets up their git repositories.
date: 2022-08-25
template: manual.html
about: https://gitlab.com/nofusscomputing
---


This page details the default setup for our repositories.

All development work that we do is tracked by a version control system, we us git.


## Git Branches

We normally utilise two protected branches `master` and `development`. However some projects might require us to support more than one version. In this case we will create a `master` and `development` for that version prefixed with the major version number, for example `development-v1`. In the case of branches prefixed with a version, the non prefixed `master` and `development` are to be considered the current version.

**master branch** This branch is considered the released version of the product. Only a `maintainer` or above can merge to this branch. No one can commit to a master branch. Our [CI]() automatically builds, git tags and creates a release of the product when a merge occurs on this branch. Only merge requests from the `development` branch will occur on this branch.

**development branch** This branch is the working branch of the product. No one except a `maintainer` or above can commit to this branch. It's not intended that any commits would be done to this branch. All merge requests to this branch will come from your or a developers `feature` branch.

**feature branch** This is the branch where all of the work that a `contributor` or `developer` conducts will occur on. The feature branch is created in your fork of the repository. When creating this branch, use the `development` branch as the source branch. Naming for a feature branch is as follows:

if the contribution is related to an issue: `{issue number}-my-feature-name` or `{issue-number}-bug-fix-in-area`

If the contribution is not related to an issue: `name-of-the-feature`

Please ensure that the `feature` branch name is relevant to what you are doing and contains no spaces.

!!! note
    you are expected to clear any merge conflicts prior to your merge request being accepted.

Further information on how how branches are setup can be found in [repository setup](repositories.md)


## Local Development Machine Setup


### Git

This section will describe the suggested git client setup.

Set your user and email

=== "Linux"

    ``` bash
    git config --global user.name "Your Name"
    git config --global user.email "youremail@yourdomain.com"
    ```

=== "Windows"

   ToDo: This section still requires some work.


!!! tip
    If you work on other repositories with different user details, remove the option `--global` from the git commands.


### VSCodium / VSCode

VSCodium is the open source version of Microsofts VSCode. Even though both of these softwares will work, we recommend VSCodium. Throughout our documentation, VSCode and VSCodium will be used interchangebly even though we intend to say VSCodium.

To aid in ensuring the development environment is the same across multiple contributor/developers computers, We setup the repository for VSCodium. To confirm if the repository that your working on supports VSCodium/VSCode, you should see a `.vscode` directory within the root of the repository.


## Directory Structure

The Following files and directories are required for our repositories. for an explaination of each item in the structure, click the corresponding '+' next to it.

``` bash title="bash> ls -la"

{repo_root_directory}
  |-- gitlab-ci #(1)
  |-- .gitignore #(2)
  |-- .gitlab #(3)
  |-- .gitlab-ci.yml #(4)
  |-+ .vscode #(5)
    |-- extensions.json #(6)
    |-- settings.json #(7)
    |-- tasks.json #(8)
  |-- pages # (9)

```

1. **Directory, (git sub-module)** a clone of our [gitlab-ci](../../../projects/gitlab-ci/) template repo for running CD/CI jobs.
2. **File**, a
3. **File**, a
4. **Directory,** contains gitlab related sub directories. i.e. issue/MR tempaltes
5. **Directory,** contains files applicable to setting up vscode to work with the repository.
6. **File**, a
7. **File**, a
8. **File**, a
9. **Directory,** This folder is used to build static pages for publishing on our website. Please [see Website Manual](../website/publishing_to_our_website.md#section-of-website).

ToDo: continue writing rest of dir structure.


### Templates

Under each tab below, you will find the recommended templates for files.

=== ".cz.yml"

    As we use conventional commits so that our version bumping and changelog generation is automatic, this file is required for it to function. see [No Fuss Computing Gitlab-CI Templates](../../../projects/gitlab-ci)

    ToDo: update this section with a template file.


=== ".gitignore"

    ``` text title=".gitignore" linenums="1"

    # Exclude Artifacts Directory
    artifacts/

    # Exclude Build Directories
    build/

    # Exclude Environments
    # Python venv
    dev_env/
    
    whatever the pycache dir is

    whatever the npm cache is

    ```

=== "README.md"

    All projects contain a readme with a standard header. The standard header is designed to be a quick snapshot of the project.

    ToDo: break down and explain the readme head matter sections.

    ``` md title="Repository readme file head matter, README.md"

    <div align="center" width="100%">

    # No Fuss Computing - {Project Name}
    <br>
    <br>
    ![Project Status - Active](https://img.shields.io/badge/Project%20Status-Active-green?logo=gitlab&style=plastic) 
    <br>
    <br>
    This project is hosted on [Gitlab](https://gitlab.com/nofusscomputing/projects/gitlab-ci) and has a read-only copy hosted on [Github](https://github.com/NoFussComputing/gitlab-ci).
    <br>
    <br>
    <br>

    ----

    **Stable Branch:**

    ![Gitlab build status - stable](https://img.shields.io/badge/dynamic/json?color=ff782e&label=Build&query=0.status&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F28543717%2Fpipelines%3Fref%3Dmaster&logo=gitlab&style=plastic) ![branch release version](https://img.shields.io/badge/dynamic/yaml?logo=gitlab&style=plastic&label=Release&query=%24.commitizen.version&url=https%3A%2F%2Fgitlab.com%2Fnofusscomputing%2Fprojects%2Fgitlab-ci%2F-%2Fraw%2Fmaster%2F.cz.yaml) [![PyLint Score](https://img.shields.io/badge/dynamic/json?&style=plastic&logo=python&label=PyLint%20Score&query=%24.PyLintScore&url=https%3A%2F%2Fgitlab.com%2Fnofusscomputing%2Fprojects%2Fgitlab-ci%2F-%2Fjobs%2Fartifacts%2Fmaster%2Fraw%2Fartifacts%2Fvalidation%2FPyLint%2Fbadge_pylint.json%3Fjob%3DPyLint)](https://gitlab.com/nofusscomputing/projects/gitlab-ci/-/jobs/artifacts/master/file/artifacts/validation/tests/gl-code-quality-report.html?job=PyLint)

    ----

    **Development Branch** 

    ![Gitlab build status - development](https://img.shields.io/badge/dynamic/json?color=ff782e&label=Build&query=0.status&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F28543717%2Fpipelines%3Fref%3Ddevelopment&logo=gitlab&style=plastic) ![branch release version](https://img.shields.io/badge/dynamic/yaml?logo=gitlab&style=plastic&label=Release&query=%24.commitizen.version&url=https%3A%2F%2Fgitlab.com%2Fnofusscomputing%2Fprojects%2Fgitlab-ci%2F-%2Fraw%2Fdevelopment%2F.cz.yaml) [![PyLint Score](https://img.shields.io/badge/dynamic/json?&style=plastic&logo=python&label=PyLint%20Score&query=%24.PyLintScore&url=https%3A%2F%2Fgitlab.com%2Fnofusscomputing%2Fprojects%2Fgitlab-ci%2F-%2Fjobs%2Fartifacts%2Fdevelopment%2Fraw%2Fartifacts%2Fvalidation%2FPyLint%2Fbadge_pylint.json%3Fjob%3DPyLint)](https://gitlab.com/nofusscomputing/projects/gitlab-ci/-/jobs/artifacts/development/file/artifacts/validation/tests/gl-code-quality-report.html?job=PyLint)


    ----

    <br>



    ![Gitlab forks count](https://img.shields.io/badge/dynamic/json?label=Forks&query=%24.forks_count&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F28543717%2F&color=ff782e&logo=gitlab&style=plastic) ![Gitlab stars](https://img.shields.io/badge/dynamic/json?label=Stars&query=%24.star_count&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F28543717%2F&color=ff782e&logo=gitlab&style=plastic) [![Open Issues](https://img.shields.io/badge/dynamic/json?color=ff782e&logo=gitlab&style=plastic&label=Open%20Issues&query=%24.statistics.counts.opened&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F28543717%2Fissues_statistics)](https://gitlab.com/nofusscomputing/projects/gitlab-ci/-/issues) 


    ![GitHub forks](https://img.shields.io/github/forks/NoFussComputing/gitlab-ci?logo=github&style=plastic&color=000000&labell=Forks) ![GitHub stars](https://img.shields.io/github/stars/NoFussComputing/gitlab-ci?color=000000&logo=github&style=plastic) ![Github Watchers](https://img.shields.io/github/watchers/NoFussComputing/gitlab-ci?color=000000&label=Watchers&logo=github&style=plastic)
    <br>
    <br>
    <br>
    </div>
    text starts here


    ```


## Website Pages

> ToDo: fix this section.
> the directory structure of pages needs to replicate the actual site, including having placeholder index.md files. this enables the nav structure to be built.
>
> This section is better suited for the website manual with a link from repo setup. This page should still show the pages directory with a link to the website manual.

To have pages to be published to our website the following will be required to be setup within the repository.

Currently we use static pages built by mkdocs and with this setup enables a continious integration model to be used.

``` txt

{repo_root_directory}
  |--pages
       |--images
  |--mkdocs.yml
```

When building the pages for deployment to the website. the following directories are used/required:

- `pages/images`

    This folder will contain any images that will be displayed on pages.

- `pages`

    This directory is in the repository root and contains the source markdown files used as part of the build source. This path must be checked into git.

- `build`

    This directory is the output directory for mkdocs. Known as the build directory. The contents of this folder form the contents of the html to be served from a webserver. Ensure that `.gitignore` as an entry to exclude it from commiting to the repository.

    !!! important
        This folder and it's full contents need to be included in the CI Job Artifacts.

- `source`

    This folder is part of the Artifacts of the CI build job. It's created by copying the pages folder to the root of the artifacts directory.

    !!! important
        This folder and it's full contents need to be included in the CI Job Artifacts. Create with `cp build artifacts/source`. If the pages folder is modified before the build with auto-generated content, ensure that it's included before copying the build folder to the artifacts folder.


### Continious Integration



ToDo: document steps required to link to deployment of live website.
