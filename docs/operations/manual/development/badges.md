---
title: Badge usage
description: How No Fuss Computing uses badges.
date: 2022-08-28
template: manual.html
about: https://gitlab.com/nofusscomputing
---


This page details badge usage across No Fuss Computings development

ToDo: fix section up.


# Gitlab badges

Badge Services:
- [shields.io](https://shields.io)
- [badgen.net](https://badgen.net/gitlab)

> Note: in the code examples ensure everythin is on one line.

- ![Github Watchers](https://img.shields.io/github/watchers/NoFussComputing/gitlab-ci?color=000000&label=Watchers&logo=github&style=plastic)

- ![Gitlab forks count](https://img.shields.io/badge/dynamic/json?label=Forks&query=%24.forks_count&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F28543717%2F&color=ff782e&logo=gitlab&style=plastic)

- ![GitHub forks](https://img.shields.io/github/forks/NoFussComputing/gitlab-ci?logo=github&style=plastic&color=000000&labell=Forks)


- ![Gitlab stars](https://img.shields.io/badge/dynamic/json?label=Stars&query=%24.star_count&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F28543717%2F&color=ff782e&logo=gitlab&style=plastic)

- ![GitHub stars](https://img.shields.io/github/stars/NoFussComputing/gitlab-ci?color=000000&logo=github&style=plastic)

 - ![Open Issues](https://img.shields.io/badge/dynamic/json?color=ff782e&logo=gitlab&style=plastic&label=Open%20Issues&query=%24.statistics.counts.opened&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F28543717%2Fissues_statistics)

``` markdown
![Open Issues](https://img.shields.io/badge/dynamic/json?color=ff782e&logo=gitlab&style=plastic&label=Open%20Issues&query=%24.statistics.counts.opened&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F

{project_id}

%2Fissues_statistics)
```

 - ![Gitlab build status - development](https://img.shields.io/badge/dynamic/json?color=ff782e&label=Build%20[%20Development%20]&query=0.status&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F28543717%2Fpipelines%3Fref%3Ddevelopment&logo=gitlab&style=plastic)

``` markdown
![Gitlab build status - development](https://img.shields.io/badge/dynamic/json?color=ff782e&label=

Build%20[%20Development%20]

&query=0.status&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F

{project_id}

%2Fpipelines%3Fref%3D

{Branch}

&logo=gitlab&style=plastic)

```

- ![This is an active project](https://img.shields.io/badge/Project%20Status-Active-green?logo=gitlab&style=plastic)

``` markdown
![This is an active project](https://img.shields.io/badge/Project%20Status-Active-green?logo=gitlab&style=plastic)

```

- ![This project is no longer maintained](https://img.shields.io/badge/Project%20Status-No%20Longer%20Maintained-red?logo=gitlab&style=plastic)

``` markdown
![This project is no longer maintained](https://img.shields.io/badge/Project%20Status-No%20Longer%20Maintained-red?logo=gitlab&style=plastic)
```


- ![GitLab Release (latest by date)](https://img.shields.io/gitlab/v/release/nofusscomputing/projects/ansible-roles?include_prereleases&color=blue&label=Release&logo=gitlab&style=plastic)

``` markdown
![GitLab Release (latest by date)](https://img.shields.io/gitlab/v/release/

{project path}

?include_prereleases&color=blue&label=Release&logo=gitlab&style=plastic)

```

.cz.yaml vsrsion and by branch

![branch release version](https://img.shields.io/badge/dynamic/yaml?logo=gitlab&style=plastic&label=Version&query=%24.commitizen.version&url=https%3A%2F%2Fgitlab.com%2Fnofusscomputing%2Fprojects%2Fgitlab-ci%2F-%2Fraw%2Fmaster%2F.cz.yaml)

``` yaml
![branch release version](https://img.shields.io/badge/dynamic/yaml?logo=gitlab&style=plastic&label=Version&query=%24.commitizen.version&url=https%3A%2F%2Fgitlab.com%2F

{project path seperated with `%2F` not `/`}

%2F-%2Fraw%2F

{branch}

%2F.cz.yaml)

```
