---
title: Markdown Syntax
description: This page contains the syntax that is available when drafting the markdown files that will create the static pages of our site.
date: 2022-08-25
template: manual.html
about: https://nofusscomputing.com

---

Our website is created from markdown files by mkdocs. On top of the general markdown syntax, this page will detail the additional syntax available.


## Plugin enabled syntax


### admonition

source: mkdocs-material theme

- [Documentation](https://squidfunk.github.io/mkdocs-material/reference/admonitions/)

- [types](https://squidfunk.github.io/mkdocs-material/reference/admonitions/)


### attr_list

source: mkdocs-material theme

- [Documentation](https://squidfunk.github.io/mkdocs-material/setup/extensions/python-markdown/?h=#attribute-lists)


### def_list

source: mkdocs-material theme

- [Documentation](https://squidfunk.github.io/mkdocs-material/setup/extensions/python-markdown/?h=#definition-lists)


### meta

source: mkdocs-material theme

- [Documentation](https://squidfunk.github.io/mkdocs-material/setup/extensions/python-markdown/?h=#metadata)

Supports adding a yaml header to markdown documents containing metadata.


### pymdown-extensions

- [Docs](https://facelessuser.github.io/pymdown-extensions/)


#### pymdownx.details

Creates collapsible elements.

``` md

<details>

<summary>a title goes here</summary>

this is the content that you wish to collapse
</details>

```

Demo:

----

<details>

<summary>a title goes here</summary>

this is the content that you wish to collapse
</details>

----


#### pymdownx.superfences

- [Content Tabs Documentation](https://squidfunk.github.io/mkdocs-material/reference/content-tabs/)

enables content to be placed in tabbed areas.


#### pymdownx.tabbed ToDo:ENABLE ME

``` yaml

markdown_extensions:
  - pymdownx.tabbed:

```


#### pymdownx.tasklist

- [Documentation](https://facelessuser.github.io/pymdown-extensions/extensions/tasklist/)

Create markdown task lists

``` md

- [ ] a task list item

    - [ ] sub task list item

- [ ] second task list item

```

Demo

----

- [ ] a task list item

    - [ ] sub task list item

- [ ] second task list item

----


## Code Blocks

- docs <https://squidfunk.github.io/mkdocs-material/reference/code-blocks/>
- Code block snippets <https://facelessuser.github.io/pymdown-extensions/extensions/snippets/>

code blocks can use a file as the contents of the codeblock. to do this use the relative path of the file, referencing the repositoy root directory as the base path.

``` yaml title="Including an external file in a code block"
;``` yaml title="Including an external file in a code block"
;--8<-- 
;website-template/mkdocs.yml
;--8<--

or

;--8<-- "website-template/mkdocs.yml"
;```
```
**NOTE:** *Dont include the `;` in the codeblock. they exist here so that the code displays for the example.*

to add line number use `linenums="1"` on the opening of the code block

to add a title use `title=".gitlab-ci.yml"` on the opening of the code block
