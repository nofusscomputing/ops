---
title: Publishing to our Website - No Fuss Computing
description: This article details how to publish an article to our website.
date: 2022-08-25
template: manual.html
about: https://nofusscomputing.com
tags: 
  - MKDocs
  - Page
  - Publishing
  - Article
  - Help
---


Our website is statically generated from markdown files. The available markdown syntax can be viewed within our [manual](website_syntax.md). There are certain requirements that must be met so that the page is correctly generated. Where possible we use microdata where suitable. Microdata enables the page to be described in a way that enables search engines to categorise our content. Please ensure you select the most appropriate page type under step two. Doing this will ensure that the right metadata is applied which enables the build to apply the appropriate microdata.


All of our pages contain metadata. within a Markdown file, this is placed at the top of the file. All markdown files **must** contain the following as the minimum metadata:

``` bash

---
title: #(1)
description: #(2)
date: {formatted YYYY-MM-DD} #(3)
---

```

1. This is the title of the page and will be rendered during the static page construction.
2. This is the meta description of the page and is predominatly used by search engines to display a blurb about the page.
3. This is the date that the page was created.


!!! important
    _This metadata **must** be included in all markdown files. The other types below, must have that metadata appended to this metadata._

To view the additional requirements for adding page(s) to our website please see the applicable sections below.


## Single Page

Single pages are stored within our [website repo](https://gitlab.com/nofusscomputing/infrustructure/website). Create the markdown file in the location of where you wish for the page to go. Ensure that when naming the file, it's as close as possible to the metadata title.

For example for an article, create the markdown file in path `articles/{year}/{page title with '_' for spaces}.md`


=== "Blog Post"

    **FEATURE for about not yet available. CREATE it**

    Use a blog post if the topic of discussion is just a random topic that is not covered by any other type below.

    ``` yaml
    ---
    template: article.html
    type: blog
    author:  
    copyrightHolder: (optional)
    copyrightHolderType: (mandatory if copyrightHolder present. valid values Person or Organization)
    about: (optional)
    tags: (optional)
      - tagename
    ---
    ```

    !!! tip
        if the blog is about an object/item that has a url, include the `about` metatag with the items url.

=== "How to guide"

    **FEATURE not yet available. don't use how to**

    Use a how to guide if the topic to cover explains in steps how to accomplish a task. it's important that the steps are in numbered paragraphs.

    ``` yaml
    ---
    template: article.html
    type: howto
    author: 
    copyrightHolder: (optional)
    copyrightHolderType: (mandatory if copyrightHolder present. valid values Person or Organization)
    about: {url to item being discussed}
    tags: (optional)
      - tagename
    ---
    ```

    !!! note
        author can be omitted if not intended to be from any particular person.

=== "Article"

    Use an article if the topic to discuss is what would be considered news worthy. i.e. a release of a product.

    ``` yaml
    ---
    template: article.html
    type: article
    author:  
    copyrightHolder: (optional)
    copyrightHolderType: (mandatory if copyrightHolder present. valid values Person or Organization)
    about: {url to item being discussed}
    tags: (optional)
      - tagename
    ---
    ```

    !!! note
        author can be omitted if not intended to be from any particular person.


## Section

Adding a section to the website, requires it's own repository. Below will detail the additional requirements for setting up the repository for inclusion as a section to our website. In addition to the required repository [set up steps](../development/repositories.md), the following items are required as part of the repositories file system:

``` bash

{Repository root folder}
  |-+ pages #(1)
    |-- stylesheets #(2)
  |-- website-template #(3)
  |-- mkdocs.yml #(4)

```

1. A directory call `pages` which will contian the markdown files for publishing. The structure od this directory, will become the site navigation structure.
2. This directory is optional, however if you do require additional styles, include this directory with the stylesheet name of `extra.css`
3. This directory is a `git submodule` of the project [website-template](https://gitlab.com/nofusscomputing/infrastructure/website-template)
4. This `yaml` file contains the settings applicable to the repository in question.

!!! important
All directory and file names must be in lowercase and use `_` for spaces.


=== "MKDocs Build File, mkdocs.yml"


    ``` yaml title="Minimum contents of the MKDocs build file, mkdocs.yml"

    INHERIT: website-template/mkdocs.yml

    repo_name: {gitlab repository name}
    repo_url: https://gitlab.com/{gitlab repository path}
    edit_uri: '/-/ide/project/{gitlab repository path}/edit/development/-/pages/'

    nav:
    - Home: index.md

    - Articles: 

      - articles/index.md

    - Projects: 

      - projects/index.md

    - Operations:

      - operations/index.md

    - Contact Us: contact.md

    ```

!!! Tip
    If the repository your adding is not the root repository for the website, only include the default `mkdocs.yaml` contents as specified above and the navigation structure for the repository in question. This then enables the navigation table to dynamically adjust to the relevant section of the website.

=== "git ignore file Entries, .gitignore"

    ``` .git title="Additional required entries in file .gitignore"

    # Ignore build Directory
    build/
    artifacts/

    pages/tags.md
    pages/README.md
    pages/readme.md

    # ignore python dev env
    dev_env
    pycache__
    package*json

    .markdownlint-cli2.jsonc

    ```

d


=== "Project"

    A Project is one of our projects under our projects group on gitlab. each project has it's own pages that are published as it's own section on the website, within path `/projects/`. The subfolder name is the same as the gitlab project slug.


=== "Manual"

    Manuals are placed under the website path `/operations/manuals/` in there own folder, and named after the project. To add a file to our manuals, or a new manual, use the operations repository.


    ``` yaml
    ---
    template: manual.html
    about: https://gitlab.com/nofusscomputing #(1)
    ---
    ```

    1. This must link to the item the manual page is about.


## Website Template

We use a seperate repository as a website template. This repository contains all items that form part of the general formatting of the site.


### Mkdocs Build File

detail, ToDo:


``` yaml title="Default mkdocs.yml"

--8<--
website-template/mkdocs.yml
--8<--

```

### Theme Over Ride

detail, ToDo:


## Gitlab CI/CD

within our [Gitlab-ci repository](https://gitlab.com/nofusscomputing/projects/gitlab-ci), we have a template that can be included within your `.gitlab-ci.yml` file. This template automagically adds the required jobs for deployment to our website, refer to the [documentation](https://nofusscomputing.com/projects/gitlab-ci/templates/website/) on how to use.