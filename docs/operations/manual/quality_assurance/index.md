---
title: Quality Assurance
description: No Fuss Computings Development Quality Assurance Manual.
date: 2022-08-25
template: manual.html
about: https://gitlab.com/nofusscomputing

---

During development, we wish to ensure a high degree of quality. As part of our Quality Assurance, we conduct a number of tasks. please see below for further details.

Removing the number one cause of problems, human error. We automate as much of the quality assurance process as possible.


ToDo: Add nfc bot info


## Table of contents

1. Gitlab Development Environment

   1. [Gitlab Triaging](gitlab_triage.md)

   1. [Definitions](labels.md)
