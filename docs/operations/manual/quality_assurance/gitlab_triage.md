---
title: Gitlab Triaging
description: How No Fuss Computing triages gitlab issues.
date: 2022-08-25
template: manual.html
about: https://gitlab.com/nofusscomputing

---

To aid in ensuring that we not only can action, but provide feedback. We triage issues and merge requests within gitlab.

Our triaging is automatic and occurs on schedule. This removes the posibility of a human forgetting to conduct triaging. This also ensures that the triage policies that [we use](https://gitlab.com/nofusscomputing/ops/-/tree/development/triage-policies), are <u>always</u> run in accordance with the rules that we set.


## Policies

Our policies can be found on [gitlab](https://gitlab.com/nofusscomputing/ops/-/tree/development/triage-policies). we have two policies, one covers the whole of [No Fuss Computing](https://gitlab.com/nofusscomputing/ops/-/tree/development/triage-policies/.no-fuss-computing.yml) group, and the other for [projects](https://gitlab.com/nofusscomputing/ops/-/tree/development/triage-policies/.projects.yml).


### No Fuss Computing Policy

This policies purpose is carry out generic tasks, that generally will not require any input from either `author` or `assignee`.

| :red_circle: **STOP** |
|:---- |
|  _This policy is designed to run multiple times a day. This enables actions to be taken quickly that will be picked up by the project triaging policies._ |


#### No Activity on Assigned MR in 10 days

**Purpose:** _To ensure that development work continues_

This policy detects any merge request to have a reminder placed upon it for the developer/contributor to take action to complete work, or close the Merge Request. Label ~"workflow::stalled" is applied to the Merge Request.


#### issue workflow stalled

**Purpose:** _Label to denote that activity on the issue has ceased. enables adding issue to summary issue for manual intervention_

This policy checks issues that have label ~"workflow::underway" which denotes it is being worked on, and if no action has been taken in 10 days, marks the issue as ~"workflow::stalled".


#### Set Issue Weight

**Purpose:** _To enable us to prioritize effort_

This tasks sets the issue weight based off the values of the [impact](labels.md#Impact%20definitions) and [priority](labels.md#Priority%20definitions) labels. The value of the weight is determined by our [scoring table](labels.md#Scoring%20table). The weight plays a significant role for us to determine the priority of effort accross the No Fuss Computing group.


#### No Activity on Issue in 30 Days

**Purpose:** _To have each issue checked to see what further action could be done on the issue, if any including, closing if no longer applicable._

This policy creates a summary issue for action that lists all issues that have had no action within the last 30 days so that developers of a project can take action to progress the issue at hand.


### Project Policy

This policies purpose is to carry out more specific tasks that may require `author` or `assignee` input. this policy is designated to individual projects and/or repositories.


#### Summary Missing Labels

**Purpose:** _Mandatory labels provide ability to filter issues and are also used to calculate weight. A label is also useful in providing feedback when combined with [definitions](labels.md)_

This policy Creates a summary issue with issues found that do not have labels: Type, Impact and Priority, and lists them as requiring ALL of these labels


#### Issue Failed to provide feedback

**Purpose:** _It is assumed that if no feedback is provided, that the issue is no longer relevent and should be closed. This enables minimising the effort placed in to issue that are not-relevent._

This policy Creates a summary issue with issues found that have had no action in 10 days or longer, that has label ~stage::feedback required, and closes the issue after posting a comment to the issue `author`, stating they failed to provide feedback.
