---
title: Gitlab Labels
description: This page is intended to provide definitions and usage information for different items within gitlab that we implement.
date: 2022-08-25
template: manual.html
about: https://gitlab.com/nofusscomputing
---

This page is intended to provide definitions and usage information for different items within gitlab that we implement.


## Workflow

What is occuring with the issue. we will label to denote what stage of the workflow the issue is in. These labels are for developor/contributor use and should be adjusted in accordance to what is occuring on the issue/Merge Request.

1. ~"workflow::not ready"
   > no work can commence on the object as there is missing information or is blocked by another object.

1. ~"workflow::not started"
   > previous stages `define` are complete and there is no one working on the object.

1. ~"workflow::underway"
   > work has commenced on the object and is being actively being worked on

   - after `` period of inactivity, this label will be changed to ~"workflow::ceased"


1. ~"workflow::complete"
   > That the object is complete and all tasks are marked as finished. This will be added when an issue has been marked as `fixed` in a commit. Use this label if no further work needs to be done on the issue. i.e. A commit has `fixes/closes #{issue number}` in a merge request, so the issue is labeled as complete and no further effort is placed on the issue at hand.

1. ~"workflow::stalled"
   > That the object workflow has stalled. This label will be applied in accordance with [triage Policies](gitlab_triage.md)


Delete

- ~"workflow::not ready::blocked"

- ~"workflow::ready to commence"


## Stages

> A Stages denotes where we are in relation to the action being taken with an issue.

In order the stages are:

1. ~"stage::feedback required"
   > denotes that OP is required to provide further information/feedback.

   - the object will be auto closed after `to be defined` period if no feedback supplied.

1. ~"stage::planning"
   > planning the object development stage, including conducting any inquiry or research to further enable the develop stage.

1. ~"stage::develop"
   > object is being actively worked on

1. ~"stage::test"
   > ?? Is this stage needed ?? When if at all could it be used


## Type

A type denotes what the issue is related to, in essence a catagory.


- ~"type::CI / CD"
- ~"type::bug"
- ~"type::compliance"
- ~"type::documentation"
- ~"type::feature"
- ~"type::quality assurance"


## Impact / Priority

Impact and priority labels will enable issues to be scored and worked on using a score. The calculated score provides an order to work on i.e. 1=work on first then `2` and so on.

Impact is how much of an effect the issue has whereas priority is the workflow ordering.


### Scoring table

The following scoring table gives an ordered list from one to which issues should be worked on.

| Label | ~"impact::5" | ~"impact::4" | ~"impact::3" | ~"impact::2" | ~"impact::1" | ~"impact::0" |
| ------ | :---: | :---: | :---: | :---: | :---: | :---: |
|  ~"priority::5" | 1 | 2 | 3 | 4 | 5 | 26 |
|  ~"priority::4" | 6 | 7 | 8 | 9 | 10 | 27 |
|  ~"priority::3" | 11 | 12 | 13 | 14 | 15 | 28 |
|  ~"priority::2" | 16 | 17 | 18 | 19 | 20 | 29 |
|  ~"priority::1" | 21 | 22 | 23 | 24 | 25 | 30 |
|  ~"priority::0" | -- | -- | -- | -- | -- | 31 |


Label ~"impact::0" will be used for items that neither have an impact or cause an impact.

Label ~"priority::0" will be used to denote in the future sometime. i.e. work is not scheduled.


### Impact definitions

- ~"impact::5"
   > potential or actual unauthorized access, data breech/leak, security bug or privacy bug (outside of what is designed) and failed CI/CD jobs.

- ~"impact::4"
   > bugs that cause application failure with no work around, i.e. uncaught exception, buffer overflow.

- ~"impact::3"
   > bugs that cause a feature to not function correctly, has no work around.

- ~"impact::2"
   > causes very minimal issue to program, i.e has a work around

- ~"impact::1"
   > default impact. Causes no issues to the program, i.e inconvenience.


### Priority definitions

The priority lable is used by `developers` and above, to determine our priority of tackling the issue. This value is also used in determining the issue weight.


## Oustanding actions

below is a list of further tasks that we intend on completing to improve the user expierance.

- [ ] [Notify user of expectations](https://gitlab.com/nofusscomputing/ops/-/issues/12)
   |  :notepad_spiral: Description |
   |---|
   |  Another job needs to be created that is triggered as soon as an issue or merge request is created by a non-member that informs of what will happen next etc.  |

